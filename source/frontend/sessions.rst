.. _sessions:

Sessions and States
===================

The application history function uses a session/state architecture to marshall prior model queries and load them into the view. 

History
~~~~~~~

When a user submits a query, the results of that query are added to the :ref:`history` tab, to be re-loaded or deleted as required. History is stored in the Vuex store at the key ``history`` (`state.js:66 <https://bitbucket.org/tgrains/crop-frontend/src/a18f24e3222fbd96623769eb3ff30bcb81dfdf59/src/store/state.js#lines-66>`_). The currently loaded history item is pointed to by the key ``historyIndex``.

.. image:: /img/history.png
  :alt: A diagram showing the stack containing history items

A single state on the history stack stores all the values needed to re-load it into the view. Here is a single history item:

.. code-block:: json

    {
      "inputs": { },
      "outputs": { },
      "raw": { },
      "area": { },
      "time": 1624266567959,
      "tags": [ 1 ],
      "changes": {
        "distance": 0,
        "changes": []
      },
      "deleted": false,
      "index": 0
    }

Sessions
~~~~~~~~

A session is a collection of historic states, allowing the user to clear their current history and later fork/reload it from an old session. Sessions are stored in an array in the Vuex store at the key ``sessions`` (`state.js:71 <https://bitbucket.org/tgrains/crop-frontend/src/a18f24e3222fbd96623769eb3ff30bcb81dfdf59/src/store/state.js#lines-71>`_), and are indexed by ``landscape_id`` (either 101/102). Each session contains a unique ID, a timestamp, and a history stack:

.. code-block:: json

    {
        "id": "8ac1:1381:2f4b:7f15:104f:3409:2fe5",
        "opened": 1623927787122,
        "history": []
    }


.. image:: /img/session.png
  :alt: A diagram showing the stack containing session items

Just like history, the sessions array is also operated as a LIFO stack. The newest session goes at the top, with the index of ``length - 1``.

Referencing Histories
~~~~~~~~~~~~~~~~~~~~~

The sessions datastructure holds a **reference** to the current history stack. This is important because it means that when values are pushed into the history state stack, the history stack within the current session will reflect those changes: it is a pointer to the same underlying data structure. 

A variable's type in Javascript determines whether it is passed by reference or not. Primitives are passed by value. Objects are passed by reference (more or less: `it's complicated <https://stackoverflow.com/a/3638034/1681205>`_), and `arrays are objects <https://developer.mozilla.org/en-US/docs/Web/JavaScript/Guide/Indexed_collections#array_object>`_. As such, when a new session is created and pushed onto the history stack (`mutations.js:211 <https://bitbucket.org/tgrains/crop-frontend/src/a18f24e3222fbd96623769eb3ff30bcb81dfdf59/src/store/mutations.js#lines-211>`_), we store a `copy of the reference to the current history`.

This has two effects: 

 1. It makes referencing the current history easier to read (``state.history`` versus ``state.sessions[state.landscape_id].history``). 
 2. We don't have to update the copy of the data structure in the session from the current history every time a model query is made: just update the current history (`mutations.js:274 <https://bitbucket.org/tgrains/crop-frontend/src/a18f24e3222fbd96623769eb3ff30bcb81dfdf59/src/store/mutations.js#lines-274>`_) and the state history refers to the `same object`.

But, as a result, we must pay attention to deep/shallow copying of objects (to keep the `session history` and the `current history` in sync):
  
  * We **deep-copy** the history when we do not want to modify it:
     * e.g. viewing an session (read-only) from the session stack (`actions.js:460 <https://bitbucket.org/tgrains/crop-frontend/src/a18f24e3222fbd96623769eb3ff30bcb81dfdf59/src/store/actions.js#lines-460>`_) or forking it; or viewing a comment (`actions.js:331 <https://bitbucket.org/tgrains/crop-frontend/src/a18f24e3222fbd96623769eb3ff30bcb81dfdf59/src/store/actions.js#lines-331>`_). 
     * ``cloneDeep`` from `lodash <https://lodash.com/docs/4.17.15#cloneDeep>`_ is used to perform this operation in the mutation ``deepCopyHistoryFromSession`` (`mutations.js:242 <https://bitbucket.org/tgrains/crop-frontend/src/a18f24e3222fbd96623769eb3ff30bcb81dfdf59/src/store/mutations.js#lines-242>`_)

  * We **shallow-copy** the history by assigning it by reference when we want to keep the session history up-to-date with the current history:
     * e.g. when cancelling viewing an old session and returning to the normal mode of operation (`actions.js:502 <https://bitbucket.org/tgrains/crop-frontend/src/a18f24e3222fbd96623769eb3ff30bcb81dfdf59/src/store/actions.js#lines-502>`_), or when refreshing the page (``loadTopSession`` `actions.js:414 <https://bitbucket.org/tgrains/crop-frontend/src/a18f24e3222fbd96623769eb3ff30bcb81dfdf59/src/store/actions.js#lines-414>`_).
     * The mutation ``shallowCopyHistoryFromSession`` (`mutations.js:255 <https://bitbucket.org/tgrains/crop-frontend/src/a18f24e3222fbd96623769eb3ff30bcb81dfdf59/src/store/mutations.js#lines-255>`_) performs this operation.

.. note::

    When the page is refreshed, the Vuex store is defrosted from local storage by ``vuex-persistedstate``. This flattens object references, and breaks the update-by-reference paradigm described above by recreating the current and session histories as two separate objects. 

    We deal with this by calling ``loadTopSession`` on page startup (`actions.js:414 <https://bitbucket.org/tgrains/crop-frontend/src/a18f24e3222fbd96623769eb3ff30bcb81dfdf59/src/store/actions.js#lines-414>`_), which shallow copies the history from the session, re-linking it.


When a new session is created, the old session retains the pointer to the history that it tracks. An empty array ``[]`` is assigned to the current history stack, and the BAU state is pushed onto it. The new session then references this new history object.
