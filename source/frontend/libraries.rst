
Libraries
---------

The front-end uses the following main libraries:

 * `Vuex <https://vuex.vuejs.org/>`_, a state-management pattern and library, for client-side data storage and application logic;
 * `vuex-persistedstate <https://www.npmjs.com/package/vuex-persistedstate>`_, which uses HTML5 local storage to persist the vuex store client-side;
 * `Vue Router <https://router.vuejs.org/>`_ to control page routing;
 * `Axios <https://github.com/axios/axios>`_ for HTTP REST communication with the back-end server (via `vue-axios <https://www.npmjs.com/package/vue-axios>`_);
 * `ChartJS <https://www.chartjs.org/>`_ to run the visualisations (via `vue-chartjs <https://vue-chartjs.org/>`_);
 * `Materialize <https://materializecss.com/>`_ to provide Material Design CSS;
    * (Materialize should ideally be replaced with `Vuetify <https://vuetifyjs.com/en/>`_ in future, as this would simplify the template code)


This is not an exhausive list of front-end libraries: read the `package.json <https://bitbucket.org/tgrains/crop-frontend/src/master/package.json>`_ file for full details. 

