.. _newversion:

Application Versioning
----------------------

**How does the application know when a new version is available?**

The application is versioned using its `git commit hash <https://www.mikestreety.co.uk/blog/the-git-commit-hash/>`_: changes in this hash indicate to the client that a new version is available and it should reload the Vuex store from defaults. 

The commit hash is baked into the application during the CI build phase (see :ref:`ci`) using the environment variable ``VUE_APP_GIT_HASH``. The hash is picked up `in the Dockerfile <https://bitbucket.org/tgrains/crop-frontend/src/master/Dockerfile>`_ which makes it available in the environment to ``npm run build``. If building manually, rather than by CI, use the following command to bake in the commit hash::

    docker build --build-arg VUE_APP_GIT_HASH=$(git rev-parse --short HEAD) .

Version hash verification `happens automatically on page-load <https://bitbucket.org/tgrains/crop-frontend/src/a18f24e3222fbd96623769eb3ff30bcb81dfdf59/src/store/actions.js#lines-202>`_ via the Vuex action ``checkVersion``, which checks if the version hash in the store loaded from HTML5 local storage (``this.state.versionHash``) matches the hash baked into the application environment (``process.env.VUE_APP_GIT_HASH``). If it doesn't match, the existing store is discarded and the page is reloaded.


**Why is this important?**

Because the vuex store is persisted in HTML5 local storage, if the way the application uses the store changes (e.g. a change in a datastructure), this can result in errors when using a new version of the application code with an old version of the store. The versioning process protects the application from errors arising as a result of this. 