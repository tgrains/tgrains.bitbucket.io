
Components
==========

.. warning::
    
    Developing this project requires a basic understanding of the application framework, Vue.js. If you are unfamiliar with Vue, it's a good idea to `have a look at the guide <https://vuejs.org/v2/guide/>`_ for v2 of the framework before continuing.



App.vue
-------

The `App.vue <https://bitbucket.org/tgrains/crop-frontend/src/master/src/App.vue>`_ file is the top-level application component loaded first when the application loads. Other components are organised hierachically and are called by App.vue: for example, the router view loads the tabs, each tab contains a set of visualisations, which have their own components, and so on.

The ``mounted()`` function (`see documentation <https://vuejs.org/v2/api/#mounted>`_) of App.vue calls a number of Vuex `actions <https://vuex.vuejs.org/guide/actions.html>`_ and `mutations <https://vuex.vuejs.org/guide/mutations.html>`_ which control the state of the page and perform setup for the application. See the section on the :ref:`setup` for further information. 



.. _sidebar:

Sidebar
-------
The sidebar contains the variables which are possible to change within the model, and is described in `SidebarMenu.vue <https://bitbucket.org/tgrains/crop-frontend/src/master/src/components/sidebar/SidebarMenu.vue>`_.

.. container:: twocol

    .. container:: leftside

        .. image:: /img/components/sidebar.png
          :alt: The sidebar

The sidebar lists the variables which can be adjusted in the model. These are loaded from the backend via the :ref:`strings` API. See :ref:`loading` for further detail.

The sidebar includes the ``LandAreaMenu.vue`` component, which allows switching between the study areas, and the slider rows which allow configuring the amount of area taken up by a given crop or livestock.

These are comprised of two components: ``VariableRow.vue``, which describes an individual row containing a label, a slider, and a value input box; and ``VariableGroup.vue``, which can expand and collapse to render groups of VariableRows, and provides a mechanism to scale all the crops within that group with a top-level slider. 

Moving the sliders changes the number of hectares assigned to that crop. The 'maximum' value for a given crop is the existing amount, plus the area remaining. This is why moving one slider will cause the others to also update. When a slider moves, a ``change`` event fires the update code within ``onSliderChange`` (`VariableGroup.vue:161 <https://bitbucket.org/tgrains/crop-frontend/src/a18f24e3222fbd96623769eb3ff30bcb81dfdf59/src/components/sidebar/VariableGroup.vue#lines-161>`_) to recalculate totals. This is how sliders within groups update the total for the group. Conversely, moving the top-level slider for a group scales the crops within the group, maintaining their proportions.

The data model behind the sidebar is a tree data structure, built by the computed property ``orderedElements`` (`SidebarMenu.vue:331 <https://bitbucket.org/tgrains/crop-frontend/src/a18f24e3222fbd96623769eb3ff30bcb81dfdf59/src/components/sidebar/SidebarMenu.vue#lines-331>`_), which uses ``sidebar_groups`` data structure defined in the Vuex store (`state.js:153 <https://bitbucket.org/tgrains/crop-frontend/src/a18f24e3222fbd96623769eb3ff30bcb81dfdf59/src/store/state.js#lines-153>`_) to group elements by their string identifier.


Validation
~~~~~~~~~~

The sidebar component also runs form validation on inputs, which must be positive numeric integers, and not sum to an area larger than the available area. Validation is triggered by the ``@change`` event bubbled up out of the variable group component, and the ``valueChanged`` method is called to handle it. Currently, all rows are re-validated on every change.



.. _progress:

Area Progress
-------------

The ``AreaProgress.vue`` component visualises the used and remaining upland and lowland area. The colour scheme has been chosen to make the component accessible to people with colourblindness. 

.. image:: /img/components/area.png 
    :alt: A screenshot of the area progress component

Upland area is indicated in green, lowland area in blue. The length of each bar indicates the percentage of area within these. If an area does not have uplands (for example, East Anglia), the lowlands bar fills the width of the component.

The darker blue/green overlaid on top indicates the amount of that lowland/upland area which is currently cropped, according to the values in the sidebar. The component is event-driven and updates continually as the sidebar sliders are changed. Finally, a numeric display of the area is presented below the bar.



.. _tabs:

Tabs
----

``ContainerMain.vue`` holds the tabs (`ContainerMain.vue:35 <https://bitbucket.org/tgrains/crop-frontend/src/a18f24e3222fbd96623769eb3ff30bcb81dfdf59/src/components/ContainerMain.vue#lines-35>`_) and forms the main body of the application where data is visualised. The tabs themselves are loaded in by the router view (`router/index.js:42 <https://bitbucket.org/tgrains/crop-frontend/src/a18f24e3222fbd96623769eb3ff30bcb81dfdf59/src/router/index.js#lines-42>`_). The main container is also the parent of:

 * The loading overlay
 * The landscape 'jumbotron' (which displays the area and land usage)
 * Error and information message banners
 * The comments container
 * The debug container (accessed on the 'show technical information' link at the bottom of the page)

Tabs use a `Mixin <https://vuejs.org/v2/guide/mixins.html>`_ to provide short accessors for common data (`tabMixin.js <https://bitbucket.org/tgrains/crop-frontend/src/a18f24e3222fbd96623769eb3ff30bcb81dfdf59/src/components/tabs/tabMixin.js>`_). The computed properties ``chartColors()``, ``baseData()``, ``rawData()``, and ``bauData()`` are defined within this file, and referenced by all tabs. This avoids having to address data in the Vuex store with a lengthy accessor like ``this.$store.state.data.raw[this.$store.state.landscape_id]`` every time they are used.

This section will describe the tabs and their functionality.


.. _production:

Production
~~~~~~~~~~

**Route**: ``/landscape/<id>/production``

.. image:: /img/components/production.png

The production tab shows a visualisation of the percentage change from business-as-usual values, as well as figures for changes in those values.

===========  =================================================
Variable     Source
===========  =================================================
Profit       ``"profit"`` returned from :ref:`modelget` route
Calories     ``"production"`` returned from :ref:`modelget`
Area change  Calculated change in area from BAU for both
             ``cropAreas`` and ``livestockAreas``.
===========  =================================================

.. seealso:: Section ":ref:`load_bau`" in the Application Setup article contains details on the datastructure returned from the :ref:`modelget` route.

The Production tab is declared in ``TabProduction.vue`` and contains two sub-components. ``VizProduction.vue`` handles the bar chart visualisation, and is populated by the computed property ``proddata`` (`TabProduction.vue:70 <https://bitbucket.org/tgrains/crop-frontend/src/a18f24e3222fbd96623769eb3ff30bcb81dfdf59/src/components/tabs/TabProduction.vue#lines-70>`_). 

This model of using a computed property to correctly format data before handing it to a visualisation component is used throughout the application, and code which performs the reformatting task generally lives in the component layer, as it is only formatting the data for presentation rather than changing the state of the application. Base data for the visualisations lives in the Vuex store.

``DataTable.vue`` displays a table of the raw information, which is similarly formatted by the function ``prodValues`` (`TabProduction.vue:45 <https://bitbucket.org/tgrains/crop-frontend/src/a18f24e3222fbd96623769eb3ff30bcb81dfdf59/src/components/tabs/TabProduction.vue#lines-45>`_).

.. _environment:

Environment
~~~~~~~~~~~

**Route**: ``/landscape/<id>/environment``

.. image:: /img/components/environment.png

Similarly to the `Production`_ tab, the environment tab ``TabEnvironment.vue`` contains a bar chart and a data table. Data is formatted by the functions ``enviroValues()`` (`TabEnvironment.vue:66 <https://bitbucket.org/tgrains/crop-frontend/src/a18f24e3222fbd96623769eb3ff30bcb81dfdf59/src/components/tabs/TabEnvironment.vue#lines-66>`_) for the graph, which uses the ``environmentImpacts`` dataset from the Vuex store (`state.js:42 <https://bitbucket.org/tgrains/crop-frontend/src/a18f24e3222fbd96623769eb3ff30bcb81dfdf59/src/store/state.js#lines-42>`_). This data is populated by the function ``formatData()`` (`helpers.js:41 <https://bitbucket.org/tgrains/crop-frontend/src/a18f24e3222fbd96623769eb3ff30bcb81dfdf59/src/store/helpers.js#lines-41>`_) which is used by the Vuex store mutation ``setDataFromQuery`` (`mutations.js:57 <https://bitbucket.org/tgrains/crop-frontend/src/a18f24e3222fbd96623769eb3ff30bcb81dfdf59/src/store/mutations.js#lines-57>`_).

The Environment tab also uses a `Chart.js Bar Chart <https://www.chartjs.org/docs/latest/charts/bar.html>`_. The variables include the ``pesticides`` data structure, as well as the values for GHG emissions and Nitrogen leaching.

=================  =================================================
Variable           Source
=================  =================================================
Greenhouse Gas     ``"greenhouseGasEmissions"`` returned from the
                   :ref:`modelget` route
Nitrogen Leaching  ``"nLeach"`` returned from :ref:`modelget`
All other values   ``"pesticideImpacts"`` from :ref:`modelget`
=================  =================================================


.. _nutrition:

Nutrition
~~~~~~~~~

**Route**: ``/landscape/<id>/nutrition``

.. image:: /img/components/nutrition.png

The Nutrition tab ``TabNutrition.vue`` uses the Chart.js `Pie chart <https://www.chartjs.org/docs/latest/charts/doughnut.html>`_ and `Radar chart <https://www.chartjs.org/docs/latest/charts/radar.html>`_ to visualise nutrition data (and its changes from the BAU values). The Nutrition tab is the most complex in terms of visualisations.

The Pie chart uses the plugin `chartjs-plugin-piechart-outlabels <https://www.npmjs.com/package/chartjs-plugin-piechart-outlabels>`_ to render label annotations. The data is formatted by the computed property ``nutriData`` (`TabNutrition.vue:98 <https://bitbucket.org/tgrains/crop-frontend/src/a18f24e3222fbd96623769eb3ff30bcb81dfdf59/src/components/tabs/TabNutrition.vue#lines-98>`_), using ``nutritionaldelivery`` (`state.js:44 <https://bitbucket.org/tgrains/crop-frontend/src/a18f24e3222fbd96623769eb3ff30bcb81dfdf59/src/store/state.js#lines-44>`_) as its base.

A second pie chart (below the first) visualises the nutritional split recommended in the EATLancet report. The ``eatLancet`` property (`TabNutrition.vue:114 <https://bitbucket.org/tgrains/crop-frontend/src/a18f24e3222fbd96623769eb3ff30bcb81dfdf59/src/components/tabs/TabNutrition.vue#lines-114>`_) formats the data from the ``eatLancetPlate`` data, which is hard-coded in the Vuex state (`state.js:46 <https://bitbucket.org/tgrains/crop-frontend/src/a18f24e3222fbd96623769eb3ff30bcb81dfdf59/src/store/state.js#lines-46>`_)

The Radar chart visualises the distance between the current state, the BAU state, and the Eat Lancet Plate. It uses a custom logarithmic scale (`scale.radialLogarithmic.js <https://bitbucket.org/tgrains/crop-frontend/src/master/src/components/visualisations/scale.radialLogarithmic.js>`_) as a log scale is not available in Chart.js for the radar chart by default. This custom scale provides the Chart.js library with logarithmic coordinates to draw the data. It is only partially developed: the coordinate rendering is fully implemented, but logarithmic tick generation is hard-coded. 


.. _history:

History
~~~~~~~

**Route**: ``/landscape/<id>/history``

.. image:: /img/components/history.png

The history tab (``TabHistory.vue``) provides state management, and links to the Sessions modal. It uses the ``HistoryRow.vue`` component to display past states and move between them, and several Modal dialog boxes for session management (``ModalSessions.vue``), deleted history states (``ModalTrash.vue``) and 'OK/cancel' style confirm dialogues (``ModalConfirm.vue``).

For more detail on how the history system works, please see the :ref:`sessions` page.



.. _comments:

Comments
--------

.. image:: /img/components/comments.png

The comments section uses a large number of components:

==========================  =========================================
Component                   Description
==========================  =========================================
``ContainerComments.vue``   Top-level container for comments section,
                            instantiated in ``ContainerMain.vue``
                            below the tabs section
``CommentAdd.vue``          Form for leaving a comment. Includes form
                            validation logic and the ``TagSelect`` component for tagging comments.
``CommentCard.vue``         Represents a single comment.
``CommentFilter.vue``       Menu for filtering comments. Sets the 
                            filtering options in the Vuex store.
``Jdenticon.vue``           A user avatar.
``Pagination.vue``          Move between pages of comments.
``Tag.vue``                 A 'lozenge' type tag, which can be 
                            attached to comments and history states.
``TagSelect.vue``           Allows tagging of comments using an 
                            accordion-style dropdown selection menu.
==========================  =========================================

Comment cards can contain themselves, for example, where a comment is replied to. Comments are organised sequentially, in a flat list rather than a tree. However, replies can be viewed by clicking them on the comment. 

Many of these components set values which are used in the back-end logic for requesting comments from the API (see ``loadComments`` at `actions.js:215 <https://bitbucket.org/tgrains/crop-frontend/src/a18f24e3222fbd96623769eb3ff30bcb81dfdf59/src/store/actions.js#lines-215>`_) on the route :ref:`commentget`. For example, the number of comments shown is configured by the sort/filter menu, which sets how many are requested in the variable ``store.state.comments.size``. Similarly, ``CommentFilter.vue`` sets many of these option variables, which are combined into an API request payload in the Vuex action ``getCommentsPayload`` (`actions.js:247 <https://bitbucket.org/tgrains/crop-frontend/src/a18f24e3222fbd96623769eb3ff30bcb81dfdf59/src/store/actions.js#lines-247>`_).


.. note::

    The best way to understand the project is to read the source code. Next, you may wish to clone the repository for the frontend at https://bitbucket.org/tgrains/crop-frontend/src/master/ and look at how components interlink and call each other using `events <https://vuejs.org/v2/guide/events.html>`_ and `props <https://v3.vuejs.org/guide/component-props.html>`_.
