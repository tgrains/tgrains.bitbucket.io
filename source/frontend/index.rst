
Front-end
=========

The project front-end is written in JavaScript (to the `ES6 <git.io/es6features>`_ standard) using the `Vue.js <https://vuejs.org/>`_ user interface framework. `NPM <https://www.npmjs.com/>`_ is the dependency manager. 

.. image:: /img/frontend.png
  :alt: A screenshot of the TGRAINS crop modelling tool


.. toctree::
   :maxdepth: 2
   :caption: Section Contents

   Layout <layout>
   Libraries <libraries>
   Components <components>
   Application Setup <setup>
   Application Versioning <versioning>
   Sessions and States <sessions>
   Familiarisation Video <familiarisation>

