
Layout
------

The model tool project follows the standard structure for a Vue.js project: components contain visual code, and application state and logic are handled by Vuex. REST API calls to Axios therefore are run through Vuex actions. The tree below shows a descriptive listing of the project directory. Individual files are omitted::

  .
  ├── conf
  │   └── Nginx configuration file directory 
  │       (used when building the docker container)
  │
  ├── public
  │   └── Static web pages
  │
  └── src
      ├── Source code directory (for building the application)
      │
      ├── assets
      │   └── Images (svg/png) used in the frontend
      │
      ├── components
      │   ├── Top-level components stored here (anything that 
      │   │   doesn't fit into the subdirectories!). Components
      │   │   are the presentation/display layer of the project.
      │   │   While components also contain logic which controls
      │   │   their appearance, tasks which affect application state
      │   │   should ideally be outsourced to Vuex actions.
      │   │
      │   ├── comments
      │   │   └── Components for the comments section
      │   │
      │   ├── modals
      │   │   └── Modals which pop-over other page content
      │   │
      │   ├── sidebar
      │   │   └── Sidebar components (for crop/livestock areas)
      │   │
      │   ├── tabs
      │   │   └── Components which run the tabs. Used by the
      │   │       router to return pages for defined routes.
      │   │
      │   └── visualisations
      │       └── ChartJS visualisations.
      │
      ├── css
      │   └── Contains custom visual styles and Materialize overrides
      │
      ├── router
      │   └── Contains the index.js for route configuration
      │
      ├── services
      │   └── REST API logic (uses Axios)
      │
      └── store
          └── Application logic using Vuex. The file state.js holds
              the default state, mutations.js and actions.js hold the 
              application logic.




