.. _familiarisation:

Familiarisation video
---------------------

The following video may help to familiarise yourself with the use of the front-end modelling tool. It was created for the 2021 workshops to guide work groups in their exploration of ideas and issues around sustainable local food systems:

.. raw:: html

  <div class="videowrapper">
    <iframe src="https://www.youtube-nocookie.com/embed/3xsVODldh70" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
  </div>

