.. _design:

Design
======

The original brief for the tool is taken from the TGRAINS project proposal, which (among the other research questions) asks `how can IT promote the development of a regional food system that better connects food system actors to achieve healthy diets and more sustainable food production`? The crop model tool is the outcome of this exploration, a `prototype digital platform for connecting stakeholders in envisioned regional food systems`.

Based on qualitative analysis of transcribed data from three 2019 workshops in South Wales, East Anglia, and Northumberland, a set of design requirements were drafted by the Northumbria University team for a modelling tool to be used in a second set of workshops in 2021, which evaluated the prototype and gathered further qualitative data on concerns for local, sustainable and healthy food systems. Further, given the Covid-19 pandemic, the tool produced needed to be suitable for use in an online setting.

A set of storyboards were produced to consider possible features for the tool. Some of the features were retained, others dropped. One early idea was to produce a 'scaled-up' version of the model for sharing on social media, and a separate version for the workshops. Through the process of development and iterative feedback from the project team, the design moved towards the single version produced.


Storyboards
-----------

Dashboard
~~~~~~~~~

.. figure:: /img/storyboards/interface-graphs.png
  :target: _images/interface-graphs.png
  :alt: Image showing an early design for the interface

  An early sketch of the 'dashboard' style design of the tool, showing the sidebar, landscape area selection menu, crop area slider, and output graphs.

Many of the original features designed were retained, though their final design and position on the page may have changed– as is the case with the area visualisation, which was moved from a position in the sidebar to the top right corner of the main window.


Comments
~~~~~~~~
One early design requirement was that the modelling tool should have some form of social feature, to address the proposal requirement of 'connecting food system stakeholders'. Initially, this included social media log-in functionality, as seen in the `Interface Prototype`_

.. figure:: /img/storyboards/interface-comments.png
  :target: _images/interface-comments.png
  :alt: Image showing the design of the comments section

  The comments section on a mobile and desktop browser, showing how model states can be shared by users by leaving a comment. An early design for the comments section is shown.

A storyboard was produced showing the workflow for leaving a comment:

.. figure:: /img/storyboards/storyboard-comment.png
  :target: _images/storyboard-comment.png
  :alt: A storyboard with eight panels showing a user interacting with the tool to leave a comment.


Tasks
~~~~~
An early idea for a tool feature was a 'tasks' function, which would guide the user to investigate a question, and modify the model state to reach a goal. This feature did not appear in the version of the tool shown in the 2021 workshops.

.. figure:: /img/storyboards/storyboard-tasks.png
  :target: _images/storyboard-tasks.png
  :alt: A storyboard of eight panels showing the notional 'tasks' feature

This notional 'tasks' feature may be something to develop in a future 'scaled up' version of the modelling tool.


Walkthrough
~~~~~~~~~~~
A walkthrough feature was storyboarded as a way to guide users through use of the model tool. In the end, it was decided that the feature would not be a good use of development time, and a :ref:`YouTube video showing the use of the tool <familiarisation>` was recorded for use in the workshops instead.

.. figure:: /img/storyboards/storyboard-walkthrough.png
  :target: _images/storyboard-walkthrough.png
  :alt: A storyboard of eight panels showing a walkthrough of the model tool.

This could still be a useful feature in a future 'scaled up' version of the tool, as users may find this a better way of learning the tool than watching a presentation.


Workshop
~~~~~~~~
The final notional feature was a 'sync' function which would have used a message queue backend to share users' pointer locations within a 'room'. In the end, we decided that it would be easier (and take less development time) to have the facilitator share their screen in the context of the workshop, so this feature was never implemented.

.. figure:: /img/storyboards/storyboard-workshop.png
  :target: _images/storyboard-workshop.png
  :alt: A storyboard showing a 'sync' feature.

  The storyboard for the 'sync' feature shows the model tool being used as part of a workshop, and visitors joining a 'room' where their activity is shared between all clients.

Implementing this feature would have also required some re-structuring of the back-end to ensure that duplicate queries cannot be submitted by multiple clients, if there was a delay in synchronization. However, the idea that updates could be shared within a group of users remains a compelling one.


Interface Prototype
-------------------

An interface prototype was produced to demonstrate front-end look and feel using the Google `Material Design <https://material.io/design>`_ language. It consisted of drafts for UI elements (some of which changed little over the course of development). The interface prototype is available and can be viewed `by clicking the image below </frontend>`_.


.. image:: /img/prototype.png
  :alt: A screenshot showing a prototype of the model tool interface
  :width: 50%
  :target: /frontend
