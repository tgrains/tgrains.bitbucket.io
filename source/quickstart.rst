.. _quickstart:

Quick Start Guide
==================
How to pull and run the tool
-----------------------------


Dependencies
~~~~~~~~~~~~

The minimum you will require on the host machine is a working Docker instance. You can install this at https://docs.docker.com/get-docker. The tool will run on any platform supported by Docker: Windows, Linux, or Mac.

The project is structured in two halves: front-end and back-end. The project is containerized in Docker containers: the recommended way to run them is via ``docker compose``, but other container orchestration solutions such as Kubernetes can also be used if desired. 


Compose file
~~~~~~~~~~~~

The recommended way to run the tool is via ``docker compose``. You should customise the ``docker-compose.yml`` file (`found in the crop-model repository <https://bitbucket.org/tgrains/crop-model/src/master/docker-compose.yml>`_) for your own needs, but the defaults should still create a working instance. Grab this file using the following commands::

    mkdir crop-model
    cd crop-model
    curl https://bitbucket.org/tgrains/crop-model/raw/master/docker-compose.yml > docker-compose.yml
    mkdir conf
    curl https://bitbucket.org/tgrains/crop-model/raw/master/conf/redis.conf > conf/redis.conf


If you'd prefer, you can copy the example ``docker-compose.yml`` to a production compose file before customising it. The addition of ``-f my-compose-file.yml`` to the following commands will specify which file to use. 

The ``.env`` file contains the environment variables used by your instance. Customise this to change passwords and so on::
    
    curl https://bitbucket.org/tgrains/crop-model/raw/master/.env > .env
    nano .env

The following commands will pull the container images from docker hub, and bring them up on your machine::

    docker compose pull
    docker compose up -d

.. note::

    New versions of docker have the command ``docker compose``. If you see the message ``docker: 'compose' is not a docker command`` on your machine, then try `installing the stand-alone compose tool <https://docs.docker.com/compose/install/>`_. Replacing ``docker compose`` with ``docker-compose`` in the following commands will use the legacy tool. This nomenclature is used throughout the documentation so be aware of the presence/absence of a hyphen in your commands.

You can then access the tool on ``localhost``, by default on port 8080. ``http://localhost:8080/``. Customise the compose file to use a different port.

By default, the tool will run with one instance of the model backend. On a machine with multiple cores/threads you can run multiple instances of the model using::

    docker compose up -d --scale celery=8

This is useful in a production environment where you need to serve multiple users.


Cloning for development
~~~~~~~~~~~~~~~~~~~~~~~

You don't necessarily need to pull the whole git repository to run the tool. The minimum you need is the ``docker-compose.yml`` file, the ``.env`` file for environment variables, and the ``conf/`` directory (including ``redis.conf``). But, if you do want to clone the repository (which will get you these files, and much more), run::

    git clone git@bitbucket.org:tgrains/crop-model.git

