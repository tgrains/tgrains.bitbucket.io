.. _common-issues:

Common Issues
-------------

.. _e500:

500 Internal Server Error
~~~~~~~~~~~~~~~~~~~~~~~~~

The following 500 error will appear if the project backend could not be found at all:

.. image:: img/500.png
    :alt: A problem occurred. Loading strings failed. Error: Request failed with status code 500

To debug this error, check the docker containers for the backend are running and their ports are correctly configured. This occurrs when the ``/api/`` route can't find a server to process the request.

As loading strings is the first task done when communicating with the backend (as described in :ref:`setup`), the error is thrown when this task fails, hence the mention of strings in the error message.


.. _e504:

504 Gateway Timeout error
~~~~~~~~~~~~~~~~~~~~~~~~~

.. image:: img/504.png
  :alt: A Problem Occurred. Comment load failed. Error: Request failed with status 504.


A common error when starting the application: it usually means that the Flask application server for the API has not yet started. Due to the way the BAU state averages are calculated, the gunicorn server for the Python Flask application serving the API will not start until BAU calculations are finished. This can be determined as the cause of the error by running ``docker compose logs -f gunicorn`` to check if model tasks are being continually run. The logs will display the number of tasks completed/running::


    gunicorn_1  | [2021-06-28 12:55:07 +0000] [331] [INFO] 98 of 128 tasks completed...


The number of BAU calculations to run is set via the environment variable ``BAU_PRECALC_RUNS``. The default is 2. Note that this variable must also be balanced with ``BAU_PRECALC_TIMEOUT``. If ``BAU_PRECALC_TIMEOUT`` (default 300 seconds) is set too low, the celery tasks will be cancelled continually and the server may fail to start.

To resolve the error, either set the timeout value higher (e.g. 600s, or even higher on a server with reduced resources), or the number of precalc runs lower. Then wait for the BAU calculations to conclude (watch the docker log) before testing if the API is now functional. The calculation is finished when the log displays::


    [INFO] BAU precalculation for landscape 102 stored in Redis


.. _maintenance:

Maintenance Tasks
-----------------

Flushing the BAU calculation cache
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Following an update it may be desirable to flush the BAU calculation cache (e.g. if a change has been made to the model which changes the business-as-usual state. To do this, log into the running Redis container using the container ID and configured password (the default is ``devpassword``). The container ID can be retrieved by running::

    (base) [master *] sjmf2:~/crop-model$ docker ps | grep redis
    d7fe1c0fed45        redis:alpine    "docker-entrypoint.s…"  2 hours ago    Up 2 hours    0.0.0.0:6379->6379/tcp    crops_redis


Then, the container ID can be used to log into the Redis instance. Issue the ``AUTH`` command (with password) to authenticate, then the ``FLUSHALL`` command to drop all lines from the redis instance. This will drop *all* redis keys from the store::


    (base) [master *] sjmf2:~/crop-model$ docker exec -it d7fe1c0fed45 redis-cli
    127.0.0.1:6379> AUTH [password]
    OK
    127.0.0.1:6379> FLUSHALL
    OK
