.. _routes:

API Routes
==========

`/ (index) <https://model.tgrains.com/api/>`__
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

*Method:* ``GET``

Return the help string (the API documentation)


.. _test:

`/test <https://model.tgrains.com/api/test>`__
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

*Method:* ``GET``

Returns a HTML test form which can be used to POST values to the `/model` endpoint.

.. note::
  Enabled **only** when the environment variable ``FLASK_ENV=development`` is set, and also blocked via nginx configuration. Requests for this route in production will 404.


.. _echo:

`/echo <https://model.tgrains.com/api/echo>`__
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

*Method:* ``POST``

Echos whatever POST request it received back at the browser. 

.. note::
  Enabled **only** when the environment variable ``FLASK_ENV=development`` is set, and also blocked via nginx configuration. Requests for this route in production will 404.


.. _status:

`/status <https://model.tgrains.com/api/status/%3Crequest_id%3E>`__
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

*Method:* ``GET``

Retrieve a result from the model following a ``303 See Other`` redirect. Requests **must** include the result ID provided in the See Other redirect, e.g. ``/status/ee46cab0-aad9-4aee-bbd7-20300af100a1``.


.. _strings:

`/strings <https://model.tgrains.com/api/strings?landscape_id=101>`__
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

*Method:* ``GET``

Get the list of crop and livestock strings. Takes a variable for landscape ID, e.g.:

``GET /strings?landscape_id=101``

The server will return a ``303 See Other`` redirect to the :ref:`status` route with a valid ID to retrieve results.

**Example return (following 303 redirect):**

.. code-block:: json

    {
      "result": {
        "crops": [
            "fruit_soft",
            "fruit_top",
            "all_peas_beans",
            "all_oats",
            "sugarbeet",
            "all_barley",
            "maize",
            "all_wheat",
            "all_vegetables",
            "all_potato",
            "osr_winter",
            "all_feed"
        ],
        "food_groups": [
            "whole_grain",
            "starchy_veg",
            "dairy",
            "animal_protein",
            "plant_protein",
            "fat",
            "sugar",
            "vegetable",
            "fruit"
        ],
        "livestock": [
            "dairy",
            "beef_lowland",
            "chicken",
            "pork",
            "eggs",
            "lamb_lowland"
        ]
      },
      "state": "SUCCESS",
      "status": ""
    }



.. _modelget:

`/model <https://model.tgrains.com/api/model?landscape_id=101>`__
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

*Method:* ``GET``

Get the BAU (Business as usual) state. Takes a variable for landscape ID, e.g.:

``GET /model?landscape_id=101``

Valid landscape IDs are currently 101, 102.


**Example return:**

.. code-block:: json

    {
        "method": "AVERAGE",
        "result": {
            "cropAreas": [
                1673.3372840000002,
                1909.0199859999998,
                15224.47737773804,
                6865.5535579734415,
                26351.40512303984,
                56609.272291420435,
                11141.525948672726,
                80103.12507945269,
                24663.432770018826,
                14711.887185621968,
                37126.64772653286,
                52135.08946744139
            ],
            "errorFlag": 0,
            "grazingProps": {
                "beef": 0.627753842680564,
                "lamb": 0.666497397486353
            },
            "greenhouseGasEmissions": 795492.3018168507,
            "healthRiskFactors": [],
            "livestockAreas": [
                3298.282538431951,
                15639.629566959595,
                107.90023290830004,
                19726.263711066058,
                282.0148222160178,
                2890.565651266223
            ],
            "maxCropArea": 406437.86137000006,
            "maxUplandArea": 0.0,
            "myUniqueLandscapeID": 101,
            "nLeach": 31798.744603801184,
            "nutritionaldelivery": [
                0.6995718994272037,
                0.07985055549438977,
                0.0054798613793983104,
                0.061875959827172845,
                0.00812824807147386,
                0.07583032250810177,
                0.028991434456318015,
                0.03115096589807321,
                0.009120752937868352
            ],
            "pesticideImpacts": [
                1473818883.5396845,
                5586683423.622471,
                8601452862.727957,
                8878519344.18881,
                19092234653.574524
            ],
            "production": 3296714.8676818917,
            "profit": 317405159.4621084
        },
        "state": "SUCCESS",
        "status": ""
    }


.. _modelpost:

`/model <https://model.tgrains.com/api/model>`__
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

*Method:* ``POST``

Post variables to the model for response. POST body MUST include all the below variables, formatted as JSON. Values 
(with the exception of `landscape_id`, an integer) are in hectares and will be interpreted as float-point numbers.

 * ``landscape_id = 101|102``
 * (In the past, this route returned crop and livestock variables, which are now instead retrieved via :ref:`strings`)

A ``303 See Other`` redirect will be issued to provide the result URL on the :ref:`status` route. The result will be in the form documented above for a ``GET`` request to the route :ref:`modelget`.


.. _commentget:

`/comment <https://model.tgrains.com/api/comment?page=1&size=10>`__
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

*Method:* ``GET``

Get comments from the database in paginated form. It takes parameters for sorting, filtering, and pagination. Comments are returned as a JSON object, which may be large as each comment includes the state history for the session attached to it.

Parameters for **sorting**:

 * ``sort``: Integer. Options: [0-3] The sorting strategy to use

     0. Sort ascending (by timestamp)
     1. Sort descending (by timestamp)
     2. Sort comments by their state's distance from the BAU state 

        **Must** provide ``distance`` parameter when using this option

     3. Sort comments by their state's distance from the user's current state

        **Must** provide ``distance`` parameter when using this option

 * ``distance``: Provided if sort equals ``2`` or ``3``. The current state's distance (in hectares) from the BAU state.


Parameters for **filtering**:

 * ``filter``: Integer. Options: [0-4]

    0. All comments
    1. My comments

        **Must** include a ``user_id`` parameter

    2. Someone else's comments

        **Must** include ``user_id``

    3. By replies to a comment

        **Must** include ``reply_id``

    4. By comment tags

        **Must** include ``tags``

 * ``user_id``: Include when ``filter=1|2``
 * ``reply_id``: Include when ``filter=3``

 * ``tags``: Include when ``filter=4``. A comma-separated list of numeric tag IDs to filter by (e.g. 36,37). Must match tags retrieved via :ref:`tags`


Parameters for **pagination**:

 * ``page``: Integer. The page to load
 * ``size``: Integer. The size of a page

.. note::
  Page counter starts at 1. Requesting page 0 results in 404 not found (the server handles an error from flask-sqlalchemy)


**Example return:**


.. code-block:: json

    {
        "comments": [
            {
                "author": "Samantha Finnigan",
                "distance": 13132,
                "hash": "965344f0e20ed82c8c03324b5d247c29da4ee538349776a71e5bad35890ff712",
                "id": 6,
                "landscape_id": 102,
                "reply": null,
                "reply_id": null,
                "session": {
                    "count": 9,
                    "history": [ ],
                    "id": "4be9:1db5:e516:150d:cbec:f62f:a510",
                    "opened": 1622017590.0
                },
                "session_id": "4be9:1db5:e516:150d:cbec:f62f:a510",
                "state_index": 2,
                "tags": [
                    36,
                    37,
                    50
                ],
                "text": "Comment text goes here",
                "timestamp": 1622023923.0,
                "user_id": "8949:36c8:381c:45a1:ba52:cca2:8e94"
            }
        ],
        "filter": 4,
        "length": 1,
        "page": 1,
        "size": 4,
        "sort": 1
    }

.. note::
    The comment history array ``"history": [ ]`` has been removed from this example. It contains the data structure for a complete session. It may be desirable in future to lazy-load this data structure using a new API route when 'Load' is clicked in the interface. 


.. _commentpost:

`/comment <https://model.tgrains.com/api/comment?page=1&size=10>`__
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

*Method:* ``POST``

Post a comment to the model, to be stored in the database. POST body MUST include the below variables, formatted as 
JSON. 

 * `text`: String. The comment body.
 * `author`: String. The author's name.
 * `email`: String. The author's email address (may be NULL for social medial login)

POST body MAY also include the following optional variable:

 * `reply_id`: Integer. The comment that this comment is replying to.


.. _tags:

`/tags <https://model.tgrains.com/api/tags>`__
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

*Method:* ``GET``

Retrieve all comment tags from the database.

**Example return:** 

.. code-block:: json
    
    {
        "groups": {
            "-2": [ 2, 3 ],
            "-1": [ 1 ],
            "0": [ 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27 ],
            "1": [ 30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40, 41, 42, 43, 44, 45, 46, 47 ],
            "2": [ 50, 51, 52, 53, 54 ],
            "3": [ 55, 56, 57, 58, 59, 60 ],
            "4": [ 61, 62, 63, 64, 65, 66, 67, 68, 69, 70 ],
            "5": [ 71, 72, 73, 74, 75 ]
        },
        "tags": {
            "1": {
                "group": -1,
                "name": "Business-as-usual"
            },
            "10": {
                "group": 0,
                "name": "Small Business"
            },
            "75": {
                "group": 5,
                "name": "Diabetes"
            }
        }
    }

.. note::
    A large number of lines have been excluded from this example. In actuality, ``"tags"`` returns a tag definition for every tag specified in ``"groups"``


.. _reply:

`/reply <https://model.tgrains.com/api/reply?id=1>`__
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

*Method:* ``GET``

Retrieve a comment by ID. Used for getting replies in a comment chain.

 * `id`: Comment ID to retrieve

``GET /reply?id=4``

**Example return:**

.. code-block:: json

    {
      "author": "Samantha Finnigan", 
      "distance": 354986, 
      "hash": "c2fa15ca22dd3f41f87d698020381fe34f2a54fff202a66e8f0073c0bf9e22ea", 
      "id": 4, 
      "landscape_id": 101, 
      "reply_id": null, 
      "session_id": "4362:c7d6:e362:a153:d409:14ad:ac09", 
      "state_index": 40, 
      "tags": [ 18, 21, 30, 31, 32, 34, 35, 36, 37, 38, 39, 40, 42, 43, 45, 51, 53, 61 ], 
      "text": "Comment body goes here", 
      "timestamp": 1621519758.0, 
      "user_id": "9715:ed12:00a9:73e5:f173:3967:d974"
    }



.. _state:

`/state <https://model.tgrains.com/api/state>`__
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

*Method:* ``POST``

POST a state to the server. POST body MUST include a JSON object with the below params:

 * `session_id`: Integer.
 * `user_id`: Integer.
 * `index`: Integer.
 * `state`: JSON object containing the application state. 

POST body MAY also include the following optional variable:

 * `forked_from`: Integer. The ID of the state that this state was forked from. Used to link to the old state in the database.


.. _fork:

`/fork <https://model.tgrains.com/api/fork>`__
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

*Method:* ``POST``

Fork a session on the server side by ID. Replicates all states of that session in the database, with `forked_from` 
column set to the value of the originating session. POST body must be a JSON object with the following contents:

 * `session_id`: Integer.
 * `new_session_id`: Integer.
 * `user_id`: Integer.

