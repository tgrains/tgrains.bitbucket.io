.. _ci:

Continuous Integration
======================

CI builds are done using Bitbucket Pipelines. This document briefly describes this.

Both the `crop-model <https://bitbucket.org/tgrains/crop-model/addon/pipelines/home>`_ and `crop-frontend <https://bitbucket.org/tgrains/crop-frontend/addon/pipelines/home>`_ repositories run CI builds. The `bitbucket-pipelines.yml <https://bitbucket.org/tgrains/crop-model/src/master/bitbucket-pipelines.yml>`_ file contains the build configuration for these. Pushing to the ``master`` branch in either repository will trigger a build. 

The ``tgrains`` project on Bitbucket is tied to an Academic plan, which includes `500 minutes <https://bitbucket.org/blog/everything-you-need-to-know-about-build-minutes-in-bitbucket-pipelines>`_ of build time per month. This entails that development should be done in *development* branches, which should be merged into *master* when a build is required. Otherwise, the project risks running out of build minutes which will not be replenished until the end of the month. A ``development`` branch can be started by using::

    git checkout -b development

... and merged back into master following a ``commit`` using::

    git checkout master
    git merge development

The project admins are Samantha Finnigan, Adrian Clear and Ryan Sharp. New users can be added to the ``tgrains`` team by any of them, which will allow permission to push to the repositories and kick off Pipelines builds.

The usage balance for the account can be viewed on the Pipelines page by clicking 'Usage' in the top right-hand corner. Builds of the back-end take roughly 10 minutes, and the front-end take 1-2 minutes. However, as the memory requirements for building the project are high (both back and front-end suffer in this regard), the pipeline has been configured at double size. This uses double the build minutes.

If more build time is required, this can be addressed by using the new `runners <https://support.atlassian.com/bitbucket-cloud/docs/runners/>`_ feature to perform the build on your own machine, which does not use Bitbucket build minutes.

Build Scripts
-------------

The ``bitbucket-pipelines.yml`` file contains the build script which consists of the following lines:

.. code-block:: console

    export BACKEND_IMAGE_NAME=${DOCKER_HUB_USERNAME}/tgrains-cropmodel-backend
    docker build -t $BACKEND_IMAGE_NAME:$BITBUCKET_COMMIT -t $BACKEND_IMAGE_NAME:latest ./server/
    docker login --username $DOCKER_HUB_USERNAME --password $DOCKER_HUB_PASSWORD
    docker push $BACKEND_IMAGE_NAME

The environment variables for the DockerHub login username and app password are stored as a secret in the Bitbucket repository settings.