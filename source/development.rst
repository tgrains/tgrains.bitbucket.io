.. _development:

Development Guide
=================

While the articles on the components of the project explain about the structure of the project and include details of how these work, this page aims to provide a basic run-down of the tools required to develop this project, including links to tutorials on how to get going with them.

Prerequisite Knowledge
----------------------

It is useful to have at least some knowledge of the following tools:

 * `git <https://git-scm.com/>`_ (the source control tool)
 * `Docker <https://www.docker.com/get-started>`_ (for containerization) and docker-compose
 * `npm <https://docs.npmjs.com/downloading-and-installing-node-js-and-npm>`_ (for frontend package management)
 * `conda <https://docs.conda.io/en/latest/>`_ (for backend package management)
 * `Vagrant <https://www.vagrantup.com/>`_ (for developing the backend in a pre-defined environment)


Source Control with git
-----------------------

First and foremost, this project is version-controlled using ``git``. Git is used in many open-source projects, and is considered to be the gold-standard of source-control. The `Git documentation <https://git-scm.com/doc>`_ contains a set of getting started tutorial materials which may be of use if you have not used it before. The `cheatsheet <https://training.github.com/downloads/github-git-cheat-sheet/>`_ can be useful even if you have! Download git for your OS `from the git-scm website <https://git-scm.com/>`_

The project is hosted on `Bitbucket <https://bitbucket.org/>`_, a collaborative code management platform, similar to GitHub but geared more towards business users. https://bitbucket.org/tgrains/ is the address of the TGRAINS project on Bitbucket.

The source for the TGRAINS Crop Model Tool is contained within two repositories, for the back and front ends. To work on the user-facing code (what you see in the web browser at http://model.tgrains.com/), pull the frontend git repository. For everything behind the scenes, the backend repository is the one to grab:

.. code-block:: console

    git clone git@bitbucket.org:tgrains/crop-model.git

As described in the :ref:`ci` article, you should work on code within a git branch before merging it back into master when you want to trigger a build of the source using the `Bitbucket Pipeline <https://bitbucket.org/tgrains/crop-model/addon/pipelines/home>`_:

.. code-block:: console

    git checkout -b development

Committing a change to the code can be done through the following commands:

.. code-block:: console

    git add .
    git commit -m "A summary of your change"
    git push

And merge back into master using:

.. code-block:: console

    git checkout master
    git merge --no-ffs development

.. note::

    We use the --no-ffs switch to preserve the branch structure in the branches view, as otherwise git flattens the history tree into master!


Developing the frontend with Vue.js
-----------------------------------

The front-end code is built using vue.js. There are a couple of useful features to know about when developing this code. Firstly, package management is done using ``npm``, the Node Package Manager, which you should `install according to the documentation <https://docs.npmjs.com/downloading-and-installing-node-js-and-npm>`_. 

The file ``package-json`` in `the frontend repository <https://bitbucket.org/tgrains/crop-frontend/src/master/package.json>`_ contains details of the dependencies of the project, which can be installed by running ``npm install`` from the repository directory. A development server can be started by running ``npm run serve``. Here is an example of cloning the repository, and getting it running:

.. code-block:: console
    
    git clone git@bitbucket.org:tgrains/crop-frontend.git
    cd crop-frontend
    npm install
    npm run serve

This will start a webserver on your local machine which you can access at http://localhost:8080/. A really useful feature of ``npm run serve`` is that some code changes can be hot-reloaded into the application, so that when you save the file you are working on, the changes will show up in your web browser automagically. Code changes within the Vuex stack seem to not work with this however, likely because we persist data on the backend. The debug window can be used to clear the persisted data store, by clicking "Reload Data Store From Defaults", accessed via the "show technical information" link at the bottom of the page:

.. image:: /img/debug.png
    :alt: The debug window

This information can also be used to configure your IDE, for example `WebStorm <https://www.jetbrains.com/webstorm/>`_ to run the project, by creating a run configuration which runs ``npm run serve`` to launch the development server.


Backend Requirements for Front-end Development
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Developing the frontend requires that the backend is running. If it is not, a :ref:`e500` will be thrown. By installing Docker locally and running the ``docker-compose.yml`` file found in the `crop-model <https://bitbucket.org/tgrains/crop-model/src/master/docker-compose.yml>`_ repository, we can bring up a copy of the backend which will serve the API requests made to it.

To achieve this, the development server proxies requests made on the ``/api`` route to a server running on port 8000. This is configured in the `vue.config.js file found in the crop-frontend repository <https://bitbucket.org/tgrains/crop-frontend/src/master/vue.config.js>`_. 

It is convenient to `install Docker Desktop <https://docs.docker.com/get-docker/>`_ on your development machine, as then a complete copy of the project can be pulled and run (from within the crop-model backend repository) using the command:

.. code-block:: console

    cd crop-model
    docker compose pull
    docker compose up -d

.. warning::

    Note that this will run the entire project, including the frontend image which will be served on port ``8000``. The ``/api/`` route is served through the frontend nginx server, so this is unfortunately necessary. 

    This means that it is easy to confuse your development copy, running via ``npm run serve`` on port ``8080`` with the base copy, running via ``docker compose`` on port ``8000``. If your changes don't seem to be appearing in the web browser, the first thing to check is that you're hitting the right copy of the page.


Developing the Backend
----------------------

The backend is a mix of python and C++. The backend shared object is compiled for Linux, with some very specific version requirements for libraries. This can complicate matters, as it is not possible to run the code on just any machine. This is partially why we use Docker for containerizing the application, as it makes it possible to freeze these requirements into a container which does not rely on the libraries installed and environment configured on any given machine.

As such, a `Vagrantfile <https://bitbucket.org/tgrains/crop-model/src/master/Vagrantfile>`_ is included in the crop-model repo. The Vagrantfile is configured to use `VirtualBox <https://virtualbox.org/>`_ as its backend, with the ``ubuntu/focal64`` box. Install VirtualBox first before continuing with this guide.

.. note::
    `Vagrant <https://www.vagrantup.com/>`_ is a tool which allows the specification of a virtual machine for development, complete with all the requisite libraries. It differs from Docker in that it uses full virtual machines, which can store their state and own files, as opposed to using dispoable containers. 

`Ubuntu 20.04.2.0 LTS 'Focal Fossa' <https://releases.ubuntu.com/20.04/>`_ is a version of the Ubuntu Linux distribution supported long-term by Canonical, and contains the correct version of ``libboost-serialization1.71.0`` within its repositories. The crop model library ``libTGRAINS.so`` is built with this version and requires it to run. A possible alternative to using VirtualBox and Vagrant is to install Ubuntu Focal onto your development machine– or, if you're confident, to get the right version of the libraries onto your own linux box– at your own peril! 

The Vagrantfile includes a script which sets up the VM with a `Miniconda <https://docs.conda.io/en/latest/miniconda.html>`_ install for package management, and installs ``cppyy``, ``celery``, ``flask``, and some other development python libraries. It also installs Jupyter notebook with a C++ backend (``xeus``), and installs Docker for building and running containers.

.. note::
    Two plugins are required by the Vagrantfile: these are ``vagrant-disksize`` which alows specifying a size for the VM's disk, and ``vagrant-vbguest`` which allows management of virtualbox guests. These can be installed using:

    .. code-block:: console

        vagrant plugin install vagrant-vbguest
        vagrant plugin install vagrant-disksize

To bring up the Vagrant VM, first pull the Ubuntu Fossa box, then launch the machine:

.. code-block:: console

    vagrant box add ubuntu/focal64
    vagrant up
    vagrant ssh

This should bring up the machine and log into it with ``vagrant ssh``. Note that this may take quite some time as it is retrieving the image, and running the setup script on first run. Subsequent launches with ``vagrant up`` will be considerably quicker.

.. warning::
    VirtualBox on OSX requires a Kernel module which needs to be approved in System Preferences before running Vagrant. If you receive the error ``code NS_ERROR_FAILURE (0x80004005)``, approve the kernel module in system preferences and restart your machine.


Running the development Flask Server
------------------------------------

So you've got your VM running via Vagrant, and you're logged in. What now? Files from the repository will be mounted under ``/vagrant_data`` within the VM. The next tool to use is ``conda`` to create the environment under which the server will run. This unfortunately can't be done by the script at the VM setup stage as the vagrant data directory isn't yet mounted. But conda will be installed and present in the ``$PATH``. To create the environment and activate it, run the command:

.. code-block:: console

    cd /vagrant_data/server
    conda env create -f environment.yml
    conda activate tgrains

However, you can't yet run ``python server.py`` to serve the API as there is no MySQL database or Redis store running for it to connect to. You may also want to pull and run the celery backend, to allow BAU results to be calculated on first-run of the server. Use Docker to pull these containers as you would when developing the front end:

.. code-block:: console

    cd /vagrant_data
    docker-compose pull redis mariadb celery
    docker-compose up -d redis mariadb celery

This will use the docker-compose file to pull and run only the containers you need.

Environment variables are pulled from different places. Docker compose uses the ``.env`` file in the root of the repository to set variables. The flask server doesn't use this. So, we need to specify environment variables on the command line when running it:

.. code-block:: console

    REDIS_URL=redis://:devpassword@localhost:6379/0 python server.py

This command sets the environment variable to point the Redis servers in the right place. Note that the *celery* container uses docker's networking stack to find the DNS address ``redis``, and so requires a slightly different URL to the one required by ``server.py``, which must look for the exposed port on ``localhost`` instead. Importantly, using ``export`` to set an environment variable will result in it being picked up by Docker too, which is undesirable in this use case.

The Vagrantfile configuration will by-default forward the port ``5000`` (which server.py runs on by default) to the host. To use this version of the backend with a Vue.js frontend served from the host, simply change the port in ``vue.config.js`` for the API to port ``5000``. To change port mappings, just edit the Vagrantfile (and restart the vagrant machine).


.. note::
    If you receive any of the following errors, you need to check your environment variables are set and point flask (and celery) to the correct place:

    E.g. from ``server.py``::

        redis.exceptions.AuthenticationError: Authentication required.

    Or from celery::

        ERROR/MainProcess] consumer: Cannot connect to redis://:**@localhost:6379/0: Error 99 connecting to localhost:6379. Cannot assign requested address..



.. warning::
    ``docker-compose`` is highly sensitive to changes in directory! Always make sure you're in ``/vagrant_data`` when running docker-compose commands.

.. warning:: 
    Make sure to shut down any docker containers running on the host if developing via Vagrant, as the port assignments will likely conflict resulting in errors being printed to the console when vagrant tries to start the VM. You cannot run docker on the host and vagrant at the same time (without changing the port mappings).


To shut down docker images individually, use ``docker-compose stop``. To shut down the vagrant image, use ``vagrant halt``. As the images are configured to auto restart, they will restart along with vagrant unless told not to using ``stop``.

