.. _flask:

Flask
-----

The Flask API server is run using `Gunicorn <https://gunicorn.org/>`_, a Python WSGI HTTP server. The file `server.py <https://bitbucket.org/tgrains/crop-model/src/master/server/server.py>`__ contains definitions for the :ref:`routes`. In production, the Gunicorn server acts as a WSGI gateway and the routes are called from the front-end nginx server. This is because Flask's webserver is for development only, and is not suitable for production deployment.

The server uses the `Blueprint pattern <https://flask.palletsprojects.com/en/2.0.x/blueprints/>`_. Routes are defined using `method decorators <https://www.python.org/dev/peps/pep-0318/>`_, for example:

.. code-block:: python

    @crops.route('comment', methods=['GET'])
    def get_comments():
        # Method code here


All setup for the Flask server happens in `config.py <https://bitbucket.org/tgrains/crop-model/src/master/server/config.py>`__, where the class ``Config`` reads environment variables and sets defaults for those which are undefined.

On startup, the Flask server checks Redis to see if Business-as-usual states are stored for all landscape IDs. If not, it launches these tasks and waits for their completion before averaging them, storing them, and launching the server. This can lead to 504 errors (see :ref:`common-issues`) while waiting for results to be returned, but is acceptable as the BAU calculations need only run once. 

In future, BAU storage could moved to the SQL database instead, stored in the ``states`` table. It's currently done this way as the BAU state  feature was implemented before the database was added.

The flask server connects to Redis, for adding and checking model tasks, and to the MySQL database (by default, `MariaDB <https://mariadb.org/>`_) for storing comments, model states, and so on. The `SQLAlchemy <https://www.sqlalchemy.org/>`_ ORM is used to map Python classes into the database, and create and populate database structures on startup (if they do not already exist). 


Environment Variables
~~~~~~~~~~~~~~~~~~~~~

The following table contains the environment variables which configure the operation of the server:

=======================  =========================  ======================================================
Environment Variable     Default                    Definition
=======================  =========================  ======================================================
FLASK_ENV                development                Controls whether server runs in 'development' or 'production' mode
FLASK_RUN_PORT           5000                       Port to run Flask on. Not used when running under ``gunicorn``.
LOG_LEVEL                INFO                       Level to log messages at. Can be set to ``DEBUG`` for development.
REDIS_URL                redis://{}:6379/0          See the section :ref:`redis` in this document for how ``{}`` is defined.
SQLALCHEMY_DATABASE_URI  mysql://root
                         :devpassword
                         @mariadb/tgrains
                                                    MySQL connection credentials. Includes username (``root``), password (``devpassword``), the address to look for the mysql database (``mariadb``), and the schema to access (``tgrains``).

BAU_PRECALC_RUNS         2                          Number of runs of the model to average when creating the BAU state.
BAU_PRECALC_TIMEOUT      300                        
                                                    Time to wait for BAU averaging celery tasks. If set too short, can cause the server to fail to load.
SCRIPT_NAME              /api                       
                                                    The script name controls the path prefix used when running Flask under WSGI. See `here <https://github.com/sjmf/reverse-proxy-minimal-example>`_ for a minimal working example, based on `this article <https://dlukes.github.io/flask-wsgi-url-prefix.html>`_.
PROXY_PATH               /                          
                                                    The path `registered as the application root <https://bitbucket.org/tgrains/crop-model/src/master/server/server.py#lines-570>`_ for the server. Note that this is NOT the way to set the path when placing the server behind a reverse proxy: use ``SCRIPT_NAME`` instead.
PROXY_FIX                0                          
                                                    How many times the script is reverse-proxied. Used by the `Werkzeug ProxyFix middleware <https://werkzeug.palletsprojects.com/en/2.0.x/middleware/proxy_fix/>`_.
HASH_SALT                <a random string>          
                                                    The salt to use when generating hashes using `Python's hashlib <https://docs.python.org/3/library/hashlib.html>`_.
=======================  =========================  ======================================================


.. note::
  
  Check the `docker-compose.yml <https://bitbucket.org/tgrains/crop-model/src/master/docker-compose.yml>`_ file for environment variables that are overridden by default for the Flask server.

