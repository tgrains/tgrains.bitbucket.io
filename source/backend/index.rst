.. _backend:

Back-end
========

The back-end of the project consists of an API (using Python `Flask <https://flask.palletsprojects.com/en/2.0.x/>`_), and a task queue (`Celery <https://docs.celeryproject.org/en/stable/getting-started/introduction.html>`_) which runs compiled code for the Rothamsted TGRAINS crop model (using the `cppyy <https://cppyy.readthedocs.io/en/latest/>`__ Python-C++ bindings generator). 

Think of it a bit like a food system: the front-end is your dinner plate, and the C++ core (``libTGRAINS.so``) is the farmer's field. Everything in-between is the infrastructure which gets data (food) from one place to the other. The REST API is like a truck, the Flask server is a packing and distribution centre, the Celery task runner is a combine harvester... this is where my analogy starts to fall down. ;)

The back-end is orchestrated in a series of Docker containers:

.. image:: /img/backend.png
  :alt: A block diagram of the backend infrastructure

The diagram shows how a request is passed through the system to communicate with the client web browser. This doesn't even have to run on the same machine: for the 2021 workshops, the celery task queue ran on my desktop, scaled to twelve instances. Celery automatically load-balances any requests added to the redis instances on the public server.

Traefik in this diagram is entirely optional. It is useful to automatically request SSL certificates for the tgrains domain, to earn the 's' in 'https'. However, given that there is no password entry on the site at present, the main reason for using it was to host the TGRAINS wordpress on the same server and direct traffic to the correct containers based on DNS address.

This section breaks down each layer of the back-end application to discuss their functions.


.. toctree::
   :maxdepth: 2
   :caption: Contents:

   Flask <flask>
   Database <database>
   Redis <redis>
   Celery <celery>
   Request Example <example>


