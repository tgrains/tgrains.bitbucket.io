.. _redis:

Redis
-----

Redis is an in-memory key-value store, used in the TGRAINS crop modelling tool for BAU results storage and as a broker for Celery tasks. The environment variable ``REDIS_URL`` controls where the server looks for a redis server.

This is configured in `config.py <https://bitbucket.org/tgrains/crop-model/src/master/server/config.py#lines-30>`__, and is read by default from the environment. If ``REDIS_URL`` is not found in the environment, the Flask server when run looks for:

 * the host ``redis`` (the default name of the container in ``docker-compose.yml``) in production
 * and in development looks for a server at ``127.0.0.1``.

Similarly to the database, you can access Redis directly via a cli console:

.. code-block:: console

  $:~/crop-model$ docker-compose exec redis redis-cli
  127.0.0.1:6379> AUTH devpassword
  OK

A password is required for redis by default. Redis uses a configuration file to set this password. A default configuration is built into the container image, but is overridden by ``docker-compose`` by mounting the the configuration file. The directive ``requirepass`` sets the password for the server, and is mounted in the container using the ``volumes`` directive

.. code-block:: yaml

    volumes:
      - ./conf/redis.conf:/usr/local/etc/redis/redis.conf

Redis does not use TLS and the authentication credentials will be passed as plaintext over the network. This does not matter when all docker containers are hosted on the same machine, but still, these should therefore not be relied upon as secure. It is possible to configure redis to use TLS, but is beyond the scope of this article.

One example of where it would be desirable to access Redis over the network is to run the celery instances on another machine to scale model calculation capacity. It is desirable to limit access to the Redis port using the ``iptables`` firewall in this scenario to prevent abuse::

    iptables -D DOCKER-USER -i eth0 -p tcp --dport 6379 -j DROP
    iptables -I DOCKER-USER -i eth0 -p tcp --dport 6379 -s 192.168.1.200 -j ACCEPT

(place ``sudo`` in front of the commands if your user does not have privileges to edit the firewall)

Here, 192.168.1.200 represents the IP of a machine running the Celery task runners to allow through the firewall. Note that we use the ``DOCKER-USER`` chain to add this rule, as the other docker chains are overwritten by docker, and the usual ``INPUT`` chain `is skipped for Docker containers <https://stackoverflow.com/a/53553785/1681205>`_.


