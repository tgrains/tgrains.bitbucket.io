.. _database:

Database
--------
The database stores comments, users, states and tags. See the below schema diagram:

.. image:: /img/schema.png
  :alt: A schema diagram showing the tables in the database


Comments are stored in the ``comments`` table. A circular reference (foreign key) of ``reply_id`` links comment replies back to the `id` of the original comment. Comments are not nested. They are displayed sequentially, but can load older comments to which they are replying via the :ref:`reply` route.

Tags are loaded via the :ref:`tags` route on page load, and then stored client-side (they won't be reloaded every time the page refreshes). Comments are linked to tags on a many-to-many relationship via entries in the ``comment_tags`` table. 

Model states are stored in the `state` table and are created via the :ref:`state` route. They are linked to a user via the foreign key ``user_id``. There is no route to create a user, they are inferred and updated accordingly from POST submissions to other routes.

Comments can be linked to states by setting ``state_index`` in the ``comments`` table. States are indexed by a composite primary key of ``session_id`` and ``index``. Session IDs are generated on the client side, a randomly generated string. Due to the length of this string and the way it is generated, they are highly unlikely to conflict, but a possible future upgrade would be to either generate this ID on the server, or check for conflicts.

For database maintenance activities (for example, moderating comments) a database console can be accessed via docker:

.. code-block:: console

  #:~/crop-model$ docker-compose exec mariadb mysql -uroot -p tgrains
  Enter password: 
  Welcome to the MariaDB monitor.  Commands end with ; or \g.
  /** **/

  MariaDB [tgrains]> 

In this snippet, the command ``mysql -uroot -p tgrains`` is run in the container service ``mariadb`` using the command ``docker-compose exec`` on the host, resulting in an SQL terminal running within the container connected to the ``tgrains`` schema. This is the preferred way to access the server, rather than exposing the MySQL port 3306 publicly (although this may be desirable in a development context). 

The location of the SQL server can be configured through the environment variable ``SQLALCHEMY_DATABASE_URI``. This defaults to ``mysql://root:devpassword@127.0.0.1:3306/tgrains`` unless overridden via environment variable.

.. warning::

    If using ``localhost`` for the database URI, this should be defined as ``127.0.0.1`` as `MySQL takes 'localhost' to mean a Unix socket <https://serverfault.com/a/337928>`_ instead of TCP/IP, and the connection will fail.


