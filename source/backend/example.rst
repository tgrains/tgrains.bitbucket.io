.. _rexample:

Request Example
----------------

Here's an example request, to describe how it is passed through the infrastructure stack:

1. User clicks 'submit' in the front-end. Axios makes a ``POST`` request to the model tool server for the path ``/api/model``.
2. The request is received by NGINX via HTTP. As the request `matches the path /api <https://bitbucket.org/tgrains/crop-frontend/src/master/conf/subsite.conf#lines-52>`_ it is passed to the WSGI server, Gunicorn.
3. Gunicorn, running the Flask server, calls the function ``model_post()``, defined as the ``POST`` handler for the endpoint ``model`` by its `function decorator <https://bitbucket.org/tgrains/crop-model/src/d9755d08d3c731dc3251605e454b5f53aa63109b/server/server.py#lines-54>`_.
4. The task ``celery_model_run`` `is inserted into Redis <https://bitbucket.org/tgrains/crop-model/src/d9755d08d3c731dc3251605e454b5f53aa63109b/server/server.py#lines-60>`_ by the Celery library, along with the data provided in the ``POST`` request.
5. A response is provided to the client with a URL to check periodically for results, ``/api/status/``, with a unique ``task_id``.
6. A Celery worker receives the ``celery_model_run`` task from Redis and loads the data provided, calling the ``celery_model_run`` task (again, defined `via function decorator <https://bitbucket.org/tgrains/crop-model/src/d9755d08d3c731dc3251605e454b5f53aa63109b/server/tasks/celery.py#lines-79>`_)
7. The ``run_model`` function `is called <https://bitbucket.org/tgrains/crop-model/src/d9755d08d3c731dc3251605e454b5f53aa63109b/server/model/CropModel.py#lines-177>`_ and calls the C++ function via `cppyy`.
8. The crop model runs with the crop and livestock areas provided in the user's request. The results are returned to the Celery worker.
9. Back up the stack now. Celery places the results in Redis.
10. Meanwhile, the front-end code `has been polling <https://bitbucket.org/tgrains/crop-frontend/src/a18f24e3222fbd96623769eb3ff30bcb81dfdf59/src/services/http.js#lines-20>`_ the server for a response by a ``GET`` request on the path provided by Flask in the response to the original ``POST`` request, beginning ``api/status/`` with a task ID.
11. This is received (via nginx) and `handled by the server <https://bitbucket.org/tgrains/crop-model/src/d9755d08d3c731dc3251605e454b5f53aa63109b/server/server.py#lines-125>`_.
12. If a result from the celery task exists in Redis, this is returned to the client side, where it is displayed to the user.