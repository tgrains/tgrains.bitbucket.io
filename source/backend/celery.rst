.. _backend-celery:

Celery
------
`Celery <https://docs.celeryproject.org/en/stable/getting-started/introduction.html>`_ is a task queue, which we use to run crop modelling jobs. Jobs are defined within `celery.py <https://bitbucket.org/tgrains/crop-model/src/master/server/tasks/celery.py>`_ and called from `server.py <https://bitbucket.org/tgrains/crop-model/src/master/server/server.py>`__. There are at present three jobs which can be run via celery:

 * ``celery_get_strings`` 
     * Return name strings for the types of crops, livestock, and food groups supported by the model.
 * ``celery_model_get_bau`` 
     * Get a Business-as-usual state from the model, using the default crop/livestock area allocations.
 * ``celery_model_run`` 
     * Run the crop model using a defined set of crop and livestock areas. Areas are passed as data to the function.

The crop model itself is a compiled file, ``libTGRAINS.so``, distributed as a closed-source binary within the project repository. The `CropModel.py <https://bitbucket.org/tgrains/crop-model/src/master/server/model/CropModel.py>`_ file defines a convenience interface to this library, including some useful exceptions for when things go wrong. The `cppyy <https://cppyy.readthedocs.io/en/latest/>`__ Python-C++ bindings generator is used to allow calling the C++ function definitions from Python.

.. note::
    As the model binary is not thread-safe, Celery's `own concurrency  <https://docs.celeryproject.org/en/latest/userguide/workers.html#concurrency>`_ can't be used. This is why the `docker-compose --scale  flag <https://docs.docker.com/compose/reference/up/>`_ is used instead, as it enforces a separate instance of the binary for each container. This can be expensive on memory.

The Dockerfile which builds the Celery container defines the environment under which the binary runs. We use `Miniconda <https://docs.conda.io/en/latest/miniconda.html>`_ as the package manager for this. The library requires ``libgfortran5`` and ``libboost-serialization1.71.0`` as dependencies.


Environment Variables
~~~~~~~~~~~~~~~~~~~~~

The celery server has some environment variables of its own:

=======================  =========================  ============================================================================
Environment Variable     Default                    Definition
=======================  =========================  ============================================================================
CELERY_BROKER_URL        redis://localhost:6379/0   The URL to look for the broker for celery tasks
CELERY_RESULT_BACKEND    redis://localhost:6379/0   The backend used to store task results (same as the broker URL in our case)
=======================  =========================  ============================================================================

