.. _build:

Build and Deployment
====================

The front-end and the back-end are built separately, using `Dockerfiles <https://docs.docker.com/engine/reference/builder/>`_. These files contain the script which produces the built containers, which are then run using a `Compose file <https://docs.docker.com/compose/>`_. This section describes these scripts in detail: see the Docker references linked above for further information on the file format.


Dockerfiles
-----------

Frontend
~~~~~~~~

The `front-end Dockerfile <https://bitbucket.org/tgrains/crop-frontend/src/master/Dockerfile>`_) uses a `multi-stage build <https://docs.docker.com/develop/develop-images/multistage-build/>`_ process. This allows us to build the source into the production site, while keeping all of the libraries used to build it out of the final image (in keeping with the principle that Docker images should be minimalist).

The **build** stage of the front-end Dockerfile builds the Vue.js application for production. It uses the ``node:12-alpine`` image as a base. and embeds the ``VUE_APP_GIT_HASH`` variable (see :ref:`newversion`) into the code.

The build process is fairly simple: stripping away the Docker-specific code, we are left with a script which does this::

    npm install
    npm run build

Everything else is about making sure the files are in the right place.

The **deployment** stage of the front-end Dockerfile uses the ``nginx:alpine`` `image <https://hub.docker.com/_/nginx>`_ as a base. The built files are copied into ``/var/www/html`` from the build image (using the switch `--from=build <https://bitbucket.org/tgrains/crop-frontend/src/a18f24e3222fbd96623769eb3ff30bcb81dfdf59/Dockerfile#lines-25>`_) and the nginx configuration is copied into the image from the ``conf/subsite.conf`` file in the repository.


Backend
~~~~~~~

The `back-end Dockerfile <https://bitbucket.org/tgrains/crop-model/src/master/server/Dockerfile>`_) is slightly more complex but uses the same principle as the front-end: a multi-stage build does a deployment step and then the files are baked into the final container.

The dependencies for the backend are managed with miniconda, so we use the ``continuumio/miniconda3`` image to install a copy of all of the dependencies into a virtual environment (``conda env create``) and then pack the whole environment using the ``conda-pack`` `utility <https://conda.github.io/conda-pack/>`_. This ensures that all of the libraries are in the right place when we reference them in the production container, and avoids spurious errors from dependency libraries.

The **deployment** stage installs the libraries which are required to run the crop model library (``libgfortran5`` and ``libboost-serialization1.71.0``). The ``ubuntu:focal`` image was carefully chosen as it contains this specific version of the library: when working with compiled binary shared objects like the model, it is important to have the correct version of the libraries they were built against.

The virtual environment from the build stage is copied into ``/venv`` and the working files required by the crop model are copied into `/FieldStats` and `/Weather`, and a working directory `/OutFiles` is created. 

The backend image is unusual in that it uses a single image to provide both the ``celery`` and the ``gunicorn`` components of the tool backend (see :ref:`backend`). Gunicorn runs in the container by default, and a different command is specified to run celery. Strictly speaking, these should be built into separate images (as the flask server never needs to access the model). In this instance, however, it was simpler to combine them into a single image, which also only needs to be pulled once for both containers when running.


Compose file
------------

`Compose files <https://docs.docker.com/compose/>`_ are used to define and run multi-container Docker applications, like this one. The compose file is called ``docker-compose.yml`` and lives in the `root of the crop-model repository <https://bitbucket.org/tgrains/crop-model/src/master/docker-compose.yml>`_. It specifies what images should be pulled to run the application, how they access each other, configuration variables, and open ports.

The compose file in the repository is a good example of how to run the application. In production, the application can be run using::

    docker compose up -d --scale celery=8

This will pull the containers and bring them up on the host. When deploying in a production context, you will want to change some of the variables which are in this file. Here is an example ``docker-compose.yml`` for production:

.. code-block:: yaml

    version: '3.6'

    services:

      frontend:
        image: samfinnigan/tgrains-cropmodel-frontend:latest
        container_name: ${COMPOSE_PROJECT_NAME}_nginx
        restart: unless-stopped
        networks:
          - backend
          - default
        ports:
          - "80:80/tcp"

      gunicorn:
        image: samfinnigan/tgrains-cropmodel-backend:latest
        container_name: ${COMPOSE_PROJECT_NAME}_gunicorn
        restart: unless-stopped
        command: gunicorn --worker-tmp-dir=/dev/shm --workers=2 --threads=4 --worker-class=gthread --timeout=300 --log-file=- --bind=0.0.0.0:5000 server:app
        environment:
          - FLASK_ENV=production
          - REDIS_URL=${REDIS_URL}
          - SQLALCHEMY_DATABASE_URI=mysql://${MYSQL_USER}:${MYSQL_PASSWORD}@mariadb/${MYSQL_DATABASE}
          - BAU_PRECALC_RUNS=${BAU_PRECALC_RUNS}
          - BAU_PRECALC_TIMEOUT=${BAU_PRECALC_TIMEOUT}
          - SCRIPT_NAME=/api
          - PROXY_FIX=1 # Behind traefik, set this to 2
        expose:
          - 5000
        networks:
          - backend

      celery:
        image: samfinnigan/tgrains-cropmodel-backend:latest
        restart: unless-stopped
        command: celery -A tasks.celery worker --loglevel=INFO --concurrency=1 -- worker.prefetch_multiplier=1
        working_dir: /app/
        environment:
          - CELERY_BROKER_URL=${REDIS_URL}
          - CELERY_RESULT_BACKEND=${REDIS_URL}
        networks:
          - backend

      mariadb:
        image: mariadb
        container_name: ${COMPOSE_PROJECT_NAME}_mariadb
        restart: unless-stopped
        environment:
          - MYSQL_ROOT_PASSWORD=${MYSQL_ROOT_PASSWORD}
          - MYSQL_DATABASE=${MYSQL_DATABASE}
          - MYSQL_USER=${MYSQL_USER}
          - MYSQL_PASSWORD=${MYSQL_PASSWORD}
        command: --transaction-isolation=READ-COMMITTED --binlog-format=ROW
        networks:
          - backend
        volumes:
          - mariadb:/var/lib/mysql

      redis:
        image: redis:alpine
        container_name: ${COMPOSE_PROJECT_NAME}_redis
        restart: unless-stopped
        command: redis-server /usr/local/etc/redis/redis.conf
        networks:
          - backend
          - default
          - external
        volumes:
          - ./conf/redis.conf:/usr/local/etc/redis/redis.conf
    #    ports: 
    #      - "6379:6379/tcp"

    networks:
      backend:
      default:

    volumes:
      mariadb:

Note how the same image is used for the ``gunicorn`` and ``celery`` images. The ``command`` directive is used to override the command which is run in these containers to bring up a celery server instead, or to customise the number of workers and threads. Note that celery should not be multithreaded, as described in :ref:`backend-celery`.

A second file, ``.env`` should be placed in the same directory and populated with key-value pairs for the deployment, for example::

    REDIS_URL=redis://:password@redis:6379
    BAU_PRECALC_RUNS=128
    BAU_PRECALC_TIMEOUT=600

... and so on. An example ``.env`` file exists `in the crop-model repository <https://bitbucket.org/tgrains/crop-model/src/master/.env>`_ and can be customised for use.

.. note::
    As discussed in the :ref:`redis` article, it may be desirable to expose Redis outside the container in the instance that celery instances are run on another server. Un-comment the lines which expose the ports in the ``redis`` container to achieve this. That article contains details about how to configure the firewall to prevent potential abuse in that scenario. 