.. TGRAINS Crop Model documentation master file, created by
   sphinx-quickstart on Wed Jun 30 13:08:17 2021.
   You can adapt this file completely to your liking, but it should at least contain the root `toctree` directive.
   Turn text wrapping on to view this file.


TGRAINS Crop Model documentation
================================

The TGRAINS crop model is a tool for interacting with a mathematical model of crop distribution in two supported areas of the UK, developed as part of the `TGRAINS project <https://tgrains.com>`_ by `Samantha Finnigan <https://sjmf.in/>`_ while at `Northumbria University <https://northumbria.ac.uk>`_'s `NorSC Lab <https://nor.sc/>`_, with the guidance of Adrian Clear. The mathematical model was developed by Ryan Sharp and Alice Milne at `Rothamsted Research <https://www.rothamsted.ac.uk/>`_.

The TGRAINS crop model is an interactive visual toolset designed to make using a mathematical crop model accessible to the lay-person. The tool was used as part of a set of research workshops in June 2021 to examine how food system stakeholders interact with these kinds of tools, and investigate the potential uses and refinements which could be made towards the future.

The tool is designed to run in a containerised environment, using the built images hosted on DockerHub: one image for the `backend <https://hub.docker.com/repository/docker/samfinnigan/tgrains-cropmodel-backend/general>`_ and another for the `frontend <https://hub.docker.com/repository/docker/samfinnigan/tgrains-cropmodel-frontend/general>`_ infrastructure. See the page :ref:`quickstart` to get started.


Contents
~~~~~~~~

.. toctree::
   :maxdepth: 2
   :caption: Project Documentation:
   
   Quickstart Guide <quickstart>
   Development Guide <development>
   Back-end <backend/index>
   Front-end <frontend/index>
   API Routes <routes>
   Common Issues <common-issues>
   Build and Deployment <build>
   Continuous Integration <continuous-integration>
   Design <design>


