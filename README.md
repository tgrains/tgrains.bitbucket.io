# TGRAINS Crop Model Documentation

This repository contains the source files and documentation for the TGRAINS Crop Model.

The documentation is built with Python [Sphinx](https://docs.readthedocs.io/en/stable/intro/getting-started-with-sphinx.html). To install the requirements for building the documentation, run `pip install -r requirements.txt` from the root of this repository.

Visit [https://tgrains.bitbucket.io/](https://tgrains.bitbucket.io/) to view the documentation. 

## Updating the documentation
Make changes to the .rst files in source/ to update the documentation. The documentation is written in the ReStructuredText markup standard. To build the documentation, run 'make html'. When adding new references or pages it will sometimes be necessary to remove the /model/ directory and re-run `make html` to generate it.

Do *not* modify files under /model manually, as they will be regenerated when running `make html`.

Push to this repository with `git push` to update the documentation.


